# Hyperfine field in BCC Fe

## Experimental results

Experimental results are available in *Solid State Nuclear Magnetic Resonance 35 (2009) 25-31*.

For Fe, a 45.5 MHz signal is observed in zero-field NMR (~33 T).


## DFT estimations


A reference table is taken from *PRB 63 184413*.

![Reference table from PRB_63_184413](PRB_63_184413.png)


Results obtained with GIPAW (plane waves) and Elk are stored in the relative sub-directories and reported below. 

### Elk

The contact field obtained with **LDA is about 23 T**. With **PBE is about 25.2 T**.

### GIPAW

The contact field obtained with the following pseudopotentials is:


| Pseudopotential               | Contact Field (T) |
|:---------------------------------|:---------------------:|
| Fe.pbe-paw-gipaw-nh           | 13                |
| Fe.pbesol-spn-kjpaw_psl.1.0.0 | 33                |

