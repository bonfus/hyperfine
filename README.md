# Hyperfine

This repository hosts input and output files aimed at validating DFT based estimations of hyperfine fields in magnetic crystals.

Contributions welcome.