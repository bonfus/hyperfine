# MnP

MnP is ferromagnetic at ambient pressure when 50K < T < 290 K.

In this phase, the hyperfine field at the <sup>31</sup>P sites  <sup>55</sup>Mn sites is reported to be about 4.4 T and 11 T respectively.

## DFT

### Wien2K

WIP

### Elk

Plain GGA reproduces the experimental magnetic moment on Mn atoms quite accurately (about 1.4 mu_B) but the results for the contact hyperifne field are about twice as much as the experimental ones:

<sup>31</sup>P sites: 7.7 T
<sup>55</sup>Mn sites: 22.9 T




