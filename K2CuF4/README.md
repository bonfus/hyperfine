# Hyperfine fields in ferromagnetic K<sub>2</sub>CuF<sub>4</sub>

K<sub>2</sub>CuF<sub>4</sub> is ferromagnetic below 6.26 K. The field is reported to be along one of the a axis (ignoring the Jahn-Teller distortion).

A number of experimental results have been published.

* Ref. 1 is a short paper with NMR measurements on a powdered sample at 1.6 K.
* Ref. 2 uses a single crystal, considers the wrong structure, revises the results of Ref 1.
* Ref. 3 collects data on a powdered sample. Provides extrapolations of central lines frequencies at zero applied field at 0 K.
* Ref. 4 describes F<sup>19</sup> hyperfine fields with the correct structure. Estimates hyperfine parameters based on molecular orbital theory.
* Ref. 5 uses single crystal compounds. Measures in 1.4 T at 1.2 K.
* Ref. 6 measures at liquid He a oriented single crystal.


## Experimental results

The lattice structure is depicted below. There are 90 deg. tiled CuF octahedra along each a direction. A cleaner picture is in Ref. 5 and 6. There are two nonequivalent F sites. F atoms along *c* are labeled F<sup>19</sup><sub>c</sub>, while the atoms in the Cu plane are labeled F<sup>19</sup><sub>aI</sub> and F<sup>19</sup><sub>aII</sub>. The difference between the two depends on the direction of the local moment with respect to the F position.

![](1535261_1.jpg)
![](1535261_2.jpg)


The measured frequencies in zero field (for the central transition in the case of Cu):

| Nucleus | Ref. 3 | Ref. 1 |
|---------|:------:|:------:|
|F<sup>19</sup><sub>aI</sub> | 193.41 MHz (extrapolated from T/T<sub>c</sub>=0.3)| 208.55 MHz |
|F<sup>19</sup><sub>aII</sub> | xxx | 55 MHz |
|F<sup>19</sup><sub>c</sub> | 48 MHz | 55 MHz |
|Cu<sup>63</sup> | 159.85 MHz | 150 MHz (at 1.6 K) |

Giromagnetic rations are reported here for convenience: 

* F<sup>19</sup>: 25.181E7 rad/Ts = 40.1 MHz/T
* Cu<sup>63</sup>: 7.0974E7 rad/Ts = 11.30 MHz/T



## Principal axes

Principal axis for Cu

![](principal_axes_Cu.png)

Principal axis for F

![](principal_axes_F.png)


Hyperfine fields at Cu and F sites are reported to be

```math
\mathcal{H}=\mathcal{H}_m + \mathcal{H}_q - \gamma \hbar \mathbf{I}\cdot\mathbf{H}_e
```

```math
\mathcal{H}_m=\mathbf{S}\cdot \mathbf{A} \cdot \mathbf{I}
```

in the principal exes frame

```math
\mathbf{A}=diag(A_0 - \frac{1}{2}A^\prime, A_0 - \frac{1}{2}A^\prime,  A_0 + A^\prime)
```


The hyperfine field at the nuclear site is then given by $`\mathbf{H}=-\frac{\mathbf{S}\cdot\mathbf{A}}{\gamma \hbar}`$

### Theoretical Hyperfine Field estimates

Ref. 6 reports for Cu<sup>63</sup> $`H_{Contact}=-12.5 \,\mathrm{T}`$, $`H_{Dipolar}=-22.5 \,\mathrm{T}`$, $`H_{Orbital}=20.5 \,\mathrm{T}`$
(and field from other Cu atoms, i.e. classical dipoles on other Cu, ~0.05T).




### Measurements

An NMR measurement can only distinguish between a isotropic and an anisotropic component. This means that the orbital contribution is split into the isotropic and the anisotropic components.


#### Ref. 4 

Reports for F<sup>19</sup><sub>c</sub>

* $`H_s`$ = 2.52 T (25.2 kOe)
* $`H_p`$ = 0.91 T (9.1 kOe)

that in the nomenclature introduced above is $`H_0=2.52`$ T, $`H^\prime=1.82`$ T.


#### Ref. 5 analyses the hyperfine coupling in small applied field.

**NB: the isotropic component in Ref. 5 is very suspicious! I believe that the isotropic and the anisotropic contributions have been exchanged!**

| Nucleus | A<sub>0</sub> | A<sup>'</sup> |
|---------|--------|--------|
|F<sup>19</sup> | 191 MHz <sup> Ref. 6</sup> | 161 MHz <sup>6</sup> |
|Cu<sup>63</sup> | 48.7 MHz <sup>5</sup> | 112.6 MHz <sup>5</sup> (this is probably A<sub>0</sub>)|

Given here as frequencies according to $`\nu_0=\frac{A_0 S}{2 \pi \hbar}`$, $`\nu^\prime=\frac{A^\prime S}{2 \pi \hbar}`$.

For a spin 1/2, one gets

| Nucleus | H<sub>0</sub> | H<sup>'</sup> |
|---------|--------|--------|
|F<sup>19</sup><sub>c</sub> | 2.4 T | 2.1 T |
|F<sup>19</sup><sub>a</sub> | 2.3 T | 2.1 T |
|Cu<sup>63/65</sup> | 4.3 T  |  10 T (this is probably H<sub>0</sub>) |

Note that the classical dipolar field (from dipoles in the lattice) is negligible for the purpose of this document in both Cu and F sites (in the latter case smaller than 0.5 T).


## References

List of references used to extract values reported above.

* [1] JPSJ 30 896
* [2] JPSJ 33 979
* [3] JPSJ 36 675
* [4] JPSJ 41 1165
* [5] JPSJ 50.1109
* [6] PRB 11 4128


## DFT results

Structure used for simulations is from COD ID: 1535261. 

Input and output files are in the various sub-directories.

### Elk

Elk with PBE gives the following contact fields in T


Cu<sup>63/65</sup>: 7.5

F<sup>19</sup><sub>a</sub>: 2.9

F<sup>19</sup><sub>c</sub>: 3.7
