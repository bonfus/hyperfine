# Results on a smaller structure

The results in this directory cannot be compared with the experiment because the unit cell considered here is wrong, but has the advantage of being smaller and more symmetric than the real one, making the simulations faster and easier to check.

![](1527502.png)


Structure used for simulations is from COD ID 1527502.