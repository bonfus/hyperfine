# MnF<sub>2</sub>

I could not find much about this material yet. 

Input used for structure is COD ID: 9007535

E. D. Jones and K. B. Jefferts report a zero field frequency for Mn<sup>55</sup> of 671.4 MHz, rather close to the one obtained with Elk considering only the contact field (and modified species file with semicore put into core), i.e. 769 MHz.

See sub-directories for the details.


## References

https://link.aps.org/doi/10.1103/PhysRev.135.A1277